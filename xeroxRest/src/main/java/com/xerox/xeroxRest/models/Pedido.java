package com.xerox.xeroxRest.models;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import org.hibernate.annotations.ForeignKey;

@Entity
@Table(name = "TB_PEDIDO")
public class Pedido implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public long id;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_usuario", nullable = false)
    @ForeignKey(name = "id_usuario")
    private Usuario id_usuario = new Usuario();

    @NotNull
    private int qnt_folhas;

    @NotNull
    private String data_pedido;

    @NotNull
    private String data_entrega;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Usuario getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(Usuario id_usuario) {
        this.id_usuario = id_usuario;
    }

    public int getQnt_folhas() {
        return qnt_folhas;
    }

    public void setQnt_folhas(int qnt_folhas) {
        this.qnt_folhas = qnt_folhas;
    }

    public String getData_pedido() {
        return data_pedido;
    }

    public void setData_pedido(String data_pedido) {
        this.data_pedido = data_pedido;
    }

    public String getData_entrega() {
        return data_entrega;
    }

    public void setData_entrega(String data_entrega) {
        this.data_entrega = data_entrega;
    }

}
