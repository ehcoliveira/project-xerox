package com.xerox.xeroxRest.resourcesController;


import com.xerox.xeroxRest.models.Pedido;
import com.xerox.xeroxRest.repository.PedidoRepository;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/api")
@CrossOrigin(origins = "*")
public class PedidoResource {


    @Autowired
    PedidoRepository pedidoRepository;
    @GetMapping("/pedidos")
    @ApiOperation(value = "Retorna uma lista de pedidos")
    public List<Pedido> listarPedido(){
        return pedidoRepository.findAll();
    }

    @GetMapping("/pedido/{id}")
    @ApiOperation(value = "Retorna um pedido único")
    public Pedido listaPedidoUnico(@PathVariable(value = "id") long id){
        return  pedidoRepository.findById(id);
    }

    @PostMapping("/pedido")
    @ApiOperation(value = "Salva um pedido")
    public Pedido salvaPedido(@RequestBody @Valid Pedido pedido){
        return pedidoRepository.save(pedido);
    }

    @DeleteMapping("/pedido")
    @ApiOperation(value = "Deleta um pedido")
    public void deletaPedido(@RequestBody @Valid Pedido pedido){
        pedidoRepository.delete(pedido);
    }

    @PutMapping("/pedido")
    @ApiOperation(value = "Altera um pedido")
    public Pedido atualizaProduto(@RequestBody @Valid Pedido pedido){
        return pedidoRepository.save(pedido);
    }


}
