package com.xerox.xeroxRest.resourcesController;


import com.xerox.xeroxRest.models.Usuario;
import com.xerox.xeroxRest.repository.UsuarioRepository;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/api")
@CrossOrigin(origins = "*")
public class UsuarioResource {

    @Autowired
    UsuarioRepository usuarioRepository;
    @GetMapping("/usuarios")
    @ApiOperation(value = "Retorna uma lista de usuarios")
    public List<Usuario> listarUsuario(){
        return usuarioRepository.findAll();
    }

    @GetMapping("/usuario/{id}")
    @ApiOperation(value = "Retorna um usuario único")
    public Usuario listaUsuarioUnico(@PathVariable(value = "id") long id){
        return usuarioRepository.findById(id);
    }

    @PostMapping("/usuario")
    @ApiOperation(value = "Salva um usuario")
    public Usuario salvaUsuario(@RequestBody @Valid Usuario usuario){
        return usuarioRepository.save(usuario);
    }

    @DeleteMapping("/usuario")
    @ApiOperation(value = "Deleta um usuario")
    public void deletaUsuario(@RequestBody @Valid Usuario usuario){
        usuarioRepository.delete(usuario);
    }

    @PutMapping("/usuario")
    @ApiOperation(value = "Altera um usuario")
    public Usuario atualizaProduto(@RequestBody @Valid Usuario usuario){
        return usuarioRepository.save(usuario);
    }


}
