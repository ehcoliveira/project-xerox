package com.xerox.xeroxRest.repository;

import com.xerox.xeroxRest.models.Pedido;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PedidoRepository extends JpaRepository<Pedido, Long> {

    Pedido findById(long id);

}
