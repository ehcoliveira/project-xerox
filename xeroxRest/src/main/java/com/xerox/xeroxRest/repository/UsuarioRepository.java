package com.xerox.xeroxRest.repository;

        import com.xerox.xeroxRest.models.Usuario;
        import org.springframework.data.jpa.repository.JpaRepository;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

    Usuario findById(long id);

}
